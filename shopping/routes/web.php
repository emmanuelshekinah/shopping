<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StoreController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::prefix('login')->group(function () {
    Route::post('/userlogin', [UserController::class, 'login']);
    Route::get('/checkAuth', [UserController::class, 'checkAuth']);
    Route::get('/logout', [UserController::class, 'logout']);

});



