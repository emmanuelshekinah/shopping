<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;
use Illuminate\Support\Str;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products_category')->insert([
            'name' => Str::random(10)
        ]);
        DB::table('products_category')->insert([
            'name' => Str::random(10)
        ]);
        DB::table('products_category')->insert([
            'name' => Str::random(10)
        ]);


    }
}
