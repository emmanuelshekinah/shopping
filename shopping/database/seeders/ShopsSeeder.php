<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;
use Illuminate\Support\Str;

class ShopsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shops')->insert([
            'name' => Str::random(10),
            'location' => Str::random(10)
        ]);
        DB::table('shops')->insert([
            'name' => Str::random(10),
            'location' => Str::random(10)
        ]);
        DB::table('shops')->insert([
            'name' => Str::random(10),
            'location' => Str::random(10)
        ]);
    }
}
