<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;
use Illuminate\Support\Str;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => Str::random(10),
            'product_category_id' => 1,
            'shop_id' => 1,
            'price' => 200,
            'quantity' => 4,
        ]);
        DB::table('products')->insert([
            'name' => Str::random(10),
            'product_category_id' => 2,
            'shop_id' => 1,
            'price' => 200,
            'quantity' => 4,
        ]);
        DB::table('products')->insert([
            'name' => Str::random(10),
            'product_category_id' => 1,
            'shop_id' => 2,
            'price' => 200,
            'quantity' => 4,
        ]);
        DB::table('products')->insert([
            'name' => Str::random(10),
            'product_category_id' => 2,
            'shop_id' => 2,
            'price' => 200,
            'quantity' => 4,
        ]);
        DB::table('products')->insert([
            'name' => Str::random(10),
            'product_category_id' => 2,
            'shop_id' => 3,
            'price' => 200,
            'quantity' => 4,
        ]);
    }
}
