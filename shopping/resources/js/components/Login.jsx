import React, { useState, useEffect } from 'react'
import axios from 'axios'

const Login = () => {

    const [email, setEmail] = useState(null)
    const [password, setPassword] = useState(null)

    const onSubmit =(e)=>{
        e.preventDefault()
        console.log(email, password)

        axios.post('/login/userlogin', {email: email, password: password}).then(res=>{
            const {data} = res
            console.log(data)
            localStorage.setItem("userData", JSON.stringify(data.userData))
        }).catch((errr)=>{
            console.log(" Failed to login due to, ", errr)
        }).finally(()=>{

        })
    }

    const checkAuth = ()=>{
        axios.get('/login/checkAuth').then(res=>{
            const {data} = res
            console.log(data)

        }).catch((errr)=>{
            console.log(" Failed to login due to, ", errr)
        }).finally(()=>{

        })
    }
    const logout = ()=>{
        axios.get('/login/logout').then(res=>{
            const {data} = res
            console.log(data)

        }).catch((errr)=>{
            console.log(" Failed to logout due to, ", errr)
        }).finally(()=>{

        })
    }
    return (
        <form onSubmit={onSubmit}>

            <div className="mb-3">
                <label for="exampleInputEmail1" className="form-label">Email address</label>
                <input type="email" className="form-control" value={email} onChange={(e)=>{setEmail(e.target.value)}} required={true}/>
            </div>

            <div className="mb-3">
                <label for="exampleInputPassword1" className="form-label">Password</label>
                <input type="password" className="form-control"  value={password} onChange={(e)=>{setPassword(e.target.value)}}  required={true}/>
            </div>
            <div className="mb-3">
             <button type="submit" className="btn btn-primary">Submit</button>
            </div>
            <div className="mb-3">
            <button type="button" className="btn btn-primary" onClick={()=>{checkAuth()}}>Check Auth</button>

            </div>
            <div className="mb-3">
            <button type="button" className="btn btn-primary" onClick={()=>{logout()}}>Logout</button>

            </div>
        </form>
    )
}

export default Login
