import React from 'react';
import ReactDOM from 'react-dom';
import Login from './Login';

function Shopping() {
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-header">Shopping Application</div>

                        <div className="card-body">
                            <Login />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Shopping;

if (document.getElementById('root')) {
    ReactDOM.render(<Shopping />, document.getElementById('root'));
}
