<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Auth;

class UserController extends Controller
{
    public function login(Request $request){

        $email = $request->get('email');
        $password = $request->get('password');

        if (Auth::attempt(['email' => $email, 'password' => $password], true)) {

            return array(
                'msg'=>"Logged in",
                'status'=>200,
                'userData'=>Auth::user()
            );
        }

        return array(
            'msg'=>"failed to login",
            'status'=>500
        );
    }

    public function checkAuth(Request $request){

        if (Auth::check()) {
            return array(
                'msg'=>"user is Authenticated",
                'status'=>200,
                'userData'=>Auth::user()
            );
        }else{
            return array(
                'msg'=>"user not authenticated",
                'status'=>500
            );
        }

    }

    public function logout(Request $request){

        Auth::logout();

            return array(
                'msg'=>"user loggedout",
                'status'=>200
            );


    }

}
