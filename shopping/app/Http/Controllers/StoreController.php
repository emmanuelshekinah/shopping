<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Shops;
use Auth;

class StoreController extends Controller
{
    public static function userInfo($name){
        return Auth::user();
    }
    public function getAllStores(Request $request){

        $response = Shops::all();
        return $response;
    }

    public function createShop(Request $request){

        $shop = new Shops();
        $shop->name = $request->get('name');
        $shop->location = $request->get('location');
        $shop->save();

        $products = $request->get('products');
        return $products[0];

        if($shop->id){
            return array('msg'=>"Shop is created", 'id'=>$shop->id);
        }else{
            return array('msg'=>"Failed to create shop");
        }

    }

    public function updateShop($id, Request $request){

        $shop = Shops::find($id);

        $shop->name = $request->get('name');
        $shop->location = $request->get('location');
        $shop->save();

        if($shop->id){
            return array('msg'=>"Shop is Updated", 'id'=>$shop->id);
        }else{
            return array('msg'=>"Failed to update shop");
        }
    }

    public function deleteShop($id, Request $request){
        $shop = Shops::find($id);
        $shop->delete();

        if($shop){
            return array('msg'=>"Shop is Deleted");
        }else{
            return array('msg'=>"Failed to delete shop");
        }
    }
}
