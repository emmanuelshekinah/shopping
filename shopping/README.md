composer install

php artisan key:generate

php artisan serve
php artisan serve --port=9000

php artisan make:controller StoreController


php artisan make:migration create_shops_table

php artisan migrate

php artisan make:model Shops